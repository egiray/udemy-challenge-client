import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const UserEdit = props => {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [about, setAbout] = useState('');
  const [topics, setTopics] = useState([]);
  const [position, setPosition] = useState('');

  useEffect(() => {
    const fetchUser = id => {
      axios.get('http://localhost:3001/v1/users/' + id)
        .then(response => {
          console.log(response.data);
          const user = response.data;
          setEmail(user.email);
          (user.name) && setName(user.name);
          (user.about) && setAbout(user.about);
          (user.topics) && setTopics(user.topics);
          (user.position) && setPosition(user.position);
        })
        .catch()
    };
    props.authUser && fetchUser(props.authUser.userId);
    return () => {
      console.log("removed User component");
    };
  }, [props.authUser])

  const topicHandler = (topic) => {
    console.log("topicHandler", topics)
    if (topics.indexOf(topic) > -1) {
      const topics_ = topics.splice(topics.indexOf(topic), 1);
      setTopics(topics_);
    }
    else {
      const topics_ = [...topics, topic];
      setTopics(topics_)
    }
  };

  const updateUser = () => {
    const user = {
      name: name,
      email: email,
      // topics: topics,
      position: position,
      about: about,
    };
    var config = {
      headers: { 'Authorization': "Bearer " + props.authUser.token }
    };
    axios.put('http://localhost:3001/v1/users/' + props.authUser.userId, user, config)
      .then(response => {
        console.log(response.data);
      })
      .catch(err => {
        console.error(err);
      });
  };
  
  return (
    <section className="section has-text-centered">
      {!props.authUser &&
        <Redirect to={'/'} />
      }
      <div className="column is-4 is-offset-4 box">
        <div className="container">
          <span>User Details: {topics}</span>
          <div className="field">
            <p className="control">
              <input className="input" type="text" value={name} placeholder="Name" onChange={(e) => setName(e.target.value)} />
            </p>
          </div>
          <div className="field">
            <p className="control has-icons-left has-icons-right">
              <input className="input" type="email" value={email} placeholder="Email" onChange={(e) => setEmail(e.target.value)} />
              <span className="icon is-small is-left">
                <i className="fas fa-envelope"></i>
              </span>
              <span className="icon is-small is-right">
                <i className="fas fa-check"></i>
              </span>
            </p>
          </div>
          <div className="field">
            <p className="control">
              <input className="input" type="text" value={position} placeholder="Position" onChange={(e) => setPosition(e.target.value)} />
            </p>
          </div>
          <div className="field">
            <p className="control">
              <input className="input" type="text" value={about} placeholder="About" onChange={(e) => setAbout(e.target.value)} />
            </p>
          </div>
          <div className="field">
            <div className="control">
              <label onClick={() => topicHandler('Football')}>
                <input type="checkbox"></input>Football
              </label>
              <label onClick={() => topicHandler('Basketball')}>
                <input type="checkbox"></input>Basketball
              </label>
              <label onClick={() => topicHandler('Volleyball')}>
                <input type="checkbox"></input>Volleyball
              </label>
              <label onClick={() => topicHandler('Tennis')}>
                <input type="checkbox"></input>Tennis
              </label>
              <label onClick={() => topicHandler('Swimming')}>
                <input type="checkbox"></input>Swimming
              </label>
              <label onClick={() => topicHandler('Baseball')}>
                <input type="checkbox"></input>Baseball
              </label>
            </div>
          </div>
          <div className="field">
            <p className="control">
              <button className="button is-success is-medium is-fullwidth" onClick={updateUser}>Update</button>
            </p>
          </div>
          {props.error &&
            <div className="field">
              <p className="control">
                {props.error.message}
              </p>
            </div>
          }
        </div>
      </div>
    </section>
  )
};

const mapStateToProps = (state) => ({ authUser: state.user })

export default connect(mapStateToProps, null)(UserEdit);
