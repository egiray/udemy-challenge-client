import React, { useState } from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions';
import { Redirect } from 'react-router-dom';

const SignUp = props => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [password2, setPassword2] = useState();

  return (
    <section className="section has-text-centered">
      {props.user &&
        <Redirect to={'/users/' + props.user.userId} />
      }
      <div className="column is-4 is-offset-4 box">
        <div className="container">
          <div className="field">
            <span>Sign Up {(props.user) ? props.user.email : null}</span>
            <p className="control has-icons-left has-icons-right">
              <input className="input" type="email" placeholder="Email" onChange={(e) => setEmail(e.target.value)} />
              <span className="icon is-small is-left">
                <i className="fas fa-envelope"></i>
              </span>
              <span className="icon is-small is-right">
                <i className="fas fa-check"></i>
              </span>
            </p>
          </div>
          <div className="field">
            <p className="control has-icons-left">
              <input className="input" type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
              <span className="icon is-small is-left">
                <i className="fas fa-lock"></i>
              </span>
            </p>
          </div>
          <div className="field">
            <p className="control has-icons-left">
              <input className="input" type="password" placeholder="Password" onChange={(e) => setPassword2(e.target.value)} />
              <span className="icon is-small is-left">
                <i className="fas fa-lock"></i>
              </span>
            </p>
          </div>
          <div className="field">
            <p className="control">
              <button className="button is-success is-medium is-fullwidth" onClick={() => props.signUp(email, password, password2)}>Sign Up</button>
            </p>
          </div>
          {props.error &&
            <div className="field">
              <p className="control">
                {props.error.message}
              </p>
            </div>
          }
        </div>
      </div>
    </section>
  )
};

const mapStateToProps = (state) => ({
  user: state.user,
  error: state.error
});

const mapDispatchToProps = dispatch => ({
  signUp: (email, password, password2) => dispatch(actions.signUp(email, password, password2))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
