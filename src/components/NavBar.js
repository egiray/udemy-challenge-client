import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = props => {
  return (
    <nav className="navbar has-shadow id" role="navigation" aria-label="main navigation">
    <div className="container">
      <div className="navbar-brand">
        <a className="navbar-item" href="/">
          Company Logo
        </a>
        <button className="navbar-burger burger" aria-label="menu" aria-expanded="false">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </button>
      </div>
      <div className="navbar-end">
        <div className="navbar-item">
          <div className="buttons">
              {props.user ?
                <button className="button is-primary" to="/signout" onClick={props.signOut}>Sign Out</button>
                :
                <>
                  <Link className="button is-primary" to="/signup">Sign Up</Link>
                  <Link className="button is-link" to="/">Sign In</Link>
                </>
              }
          </div>
        </div>
      </div>
    </div>
  </nav>
  )
};

export default NavBar;
