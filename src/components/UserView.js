import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { connect } from 'react-redux';

const UserView = props => {
  const currentUserId = props.match.params.userId;
  const [user, setUser] = useState();
  console.log("logged user id:", (props.authUser) ? props.authUser.userId : "")
  const sameUser = (props.authUser) ? (props.authUser.userId === currentUserId) : false

  useEffect(() => {
    const fetchUser = () => {
      axios.get('http://localhost:3001/v1/users/' + currentUserId)
        .then(response => {
          setUser(response.data);
        })
        .catch(err => console.log(err))
    };
    fetchUser();
    return () => {
      console.log("removed User component");
    };
  }, [currentUserId]);

  let conditionalContent = <span>No user found</span>;
  if (user) {
    conditionalContent = (
      <div className="column is-4 is-offset-4 box">
        <div className="container">
          <div className="field">
            <span>User Name: {user.name}</span>
          </div>
          <div className="field">
            <span>User Email: {user.email}</span>
          </div>
          <div className="field">
            <span>User Id: {user.id}</span>
          </div>
          {sameUser &&
            <Link className="button is-primary" to="/me">Edit Profile</Link>
          }
        </div>
      </div>
    )
  }
  return (
    <section className="section has-text-centered">
      {conditionalContent}
    </section>
  )
};

const mapStateToProps = (state) => ({ authUser: state.user })

export default connect(mapStateToProps, null)(UserView);