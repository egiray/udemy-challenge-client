import axios from 'axios';
import * as actionTypes from './actionTypes';

const signInHandler = (dispatch, user) => {
  localStorage.setItem('user', JSON.stringify(user));
  dispatch({
    type: actionTypes.SIGNIN,
    user: user
  })
};

// export const checkToken = () => {
//   const email = localStorage.getItem('email');
//   const token = localStorage.getItem('token');
//   const userId = localStorage.getItem('userId');
//   if (email && token && userId) {
//     const user = {
//       email: email,
//       userId: userId,
//       token: token
//     }
//     return {
//       type: actionTypes.SIGNIN,
//       user: user
//     }
//   }
//   return {};
// };

export const signUp = (email, password, password2) => dispatch => {
  if (password !== password2) {
    dispatch({
      type: actionTypes.ERROR,
      error: {
        message: "Passwords are not same!"
      }
    });
  }
  else {
    const authData = {
      email: email,
      password: password,
      password_confirmation: password2
    };
    axios.post('http://localhost:3001/v1/auth/register', authData)
      .then(response => {
        const user = {
          email: response.data.user.email,
          userId: response.data.user.id,
          token: response.data.token.accessToken
        }
        signInHandler(dispatch, user)
      })
      .catch(err => {
        let m = (err.response) ? err.response.data.message : "Network Error";
        dispatch({
          type: actionTypes.ERROR,
          error: {
            message: m
          }
        });
      });
  };
};


// export const signIn2 = (email, password) => dispatch => {
//   if (email === 'egiray@gmx.net' && password === '123123') {
//     const user = {
//       email: 'egiray@gmx.net',
//       userId: '1245234lm234t234yl234s234',
//       token: 'tokeeeeeen123123',
//     }    
//     signInHandler(dispatch, user);
//   }
//   else {
//     dispatch({
//       type: actionTypes.ERROR,
//       error: {
//         message: 'Invalid username password'
//       }
//     });
//   }

// }


export const signIn = (email, password) => dispatch => {
  const authData = {
    email: email,
    password: password
  };
  axios.post('http://localhost:3001/v1/auth/login', authData)
    .then(response => {
      const user = {
        email: response.data.user.email,
        userId: response.data.user.id,
        token: response.data.token.accessToken
      }
      signInHandler(dispatch, user)
    })
    .catch(err => {
      let m = (err.response) ? err.response.data.message : "Network Error";
      dispatch({
        type: actionTypes.ERROR,
        error: {
          message: m
        }
      });
    });
};


export const signOut = () => {
  localStorage.removeItem('user');
  return dispatch => {
    dispatch({
      type: actionTypes.SIGNOUT,
      user: null
    });
  };
};