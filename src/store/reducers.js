import * as actionTypes from './actionTypes';

//TODO: use try
let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

const singIn = (state, action) => {
  return {
    ...state,
    user: action.user,
    loggedIn: true
  };
};

const signOut = (state, action) => {
  return {
    ...state,
    user: action.user,
    loggedIn: false
  };
};

const error = (state, action) => {
  return {
    ...state,
    error: action.error
  };
};

// export const checkToken = () => {
//   console.log("reducer checkToken");
//   try {
//     const email = localStorage.getItem('email');
//     const token = localStorage.getItem('token');
//     const userId = localStorage.getItem('userId');
//     if (email && token && userId) {
//       const user = {
//         email: email,
//         userId: userId,
//         token: token
//       }
//       return {
//         user: user
//       }
//     }
//     return null;
//   }
//   catch(err) {
//     console.err(err);
//   }
// };

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SIGNIN:
      return singIn(state, action);
    case actionTypes.SIGNOUT:
      return signOut(state, action);
    case actionTypes.ERROR:
      return error(state, action);
    // case actionTypes.CHECKTOKEN:
    //   return checkToken()
    default:
      return state;
  };
};

export default reducers;