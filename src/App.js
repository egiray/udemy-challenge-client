import React from 'react';
import { Route, Switch } from 'react-router-dom';
import SignIn from './components/SignIn';
import UserView from './components/UserView';
import UserEdit from './components/UserEdit';
import SignUp from './components/SignUp';
import NavBar from './components/NavBar';
import { connect } from 'react-redux';
import * as actions from './store/actions';

const App = props => {
  return (
    <>
      <NavBar user={props.user} signOut={props.signOut} />
      <main className="bd-main">
        <Switch>
          <Route path="/" exact component={SignIn} />
          <Route path="/signup" exact component={SignUp} />
          <Route path="/users/:userId" exact component={UserView} />
          <Route path="/me" exact component={UserEdit} />
          <Route
            component={({ location }) => {
              return (
                <div
                  style={{
                    padding: "50px",
                    width: "100%",
                    textAlign: "center"
                  }}
                >
                  The page <code>{location.pathname}</code> could not be
                  found.
                  </div>
              );
            }}
          />
        </Switch>
      </main>
    </>
  );
};

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(actions.signOut()),
});

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
